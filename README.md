# Osoy AUR Extension

Osoy extension for installing AUR packages.

Packages are downloaded into `aur` subdirectory of Osoy home.

## Installation

```bash
osoy clone gitlab.com/osoy/osoy-aur
osoy execute osoy-aur make
osoy link osoy-aur
```

## Usage

```bash
osoy aur <SUBCOMMAND>
```
