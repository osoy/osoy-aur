build:
	cargo build --release

clean:
	cargo clean

.PHONY: build clean
